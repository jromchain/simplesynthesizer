package com.chainsword.simplesynthesizer;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public class Application extends javafx.application.Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("main_screen.fxml"));
        Pane main = fxmlLoader.load();
        //main.get
        Scene scene = new Scene(main, 1000, 600);

        stage.setTitle("simpleSynthesizer");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}