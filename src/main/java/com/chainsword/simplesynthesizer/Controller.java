package com.chainsword.simplesynthesizer;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Controller {
    @FXML
    private VBox scale;
    @FXML
    private Pane topPane;
    @FXML
    private TextField speedBox = new TextField();
    @FXML
    private Pane main;
    private final ArrayList<SubFrequencyConfig> subFrequencyConfigs = new ArrayList<>(0);
    @FXML
    private GridPane subFrequencyConfigPane = new GridPane();
    @FXML
    private TextField secondsPerPlay = new TextField();
    @FXML
    private TextField samplerate = new TextField();
    IntegerProperty subFrequencyConfigCount = new SimpleIntegerProperty(0);

    public Controller() {
    }

    public void initialize() {
        //scale.scaleXProperty().bind(main.widthProperty().divide(1000));
        //scale.scaleYProperty().bind(main.heightProperty().divide(600));
        onAddSubFrequencyConfig();
        onAddSubFrequencyConfig();
        // Create a new Pane object
        //subFrequencyConfigPane = new Pane();

        // Set the size and position of the pane
        //subFrequencyConfigPane.setPrefSize(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        //subFrequencyConfigPane.setLayoutX(25);
        //subFrequencyConfigPane.setBorder(Border.stroke(Color.RED));
        //topPane.setBorder(Border.stroke(Color.RED));
        //subFrequencyConfigPane.setLayoutX(100);w
        //subFrequencyConfigPane.setLayoutY(100);
    }
    @FXML
    protected void onAddSubFrequencyConfig() {
        SubFrequencyConfig subFrequencyConfig = new SubFrequencyConfig();
        // newSlider.getTransforms().add(Transform.rotate(90,0,0));
        subFrequencyConfigCount.set(subFrequencyConfigCount.getValue()+1);
        int myNumber= subFrequencyConfigCount.getValue();
        subFrequencyConfigs.add(subFrequencyConfig);
        subFrequencyConfigPane.add(subFrequencyConfig,myNumber,1);
        subFrequencyConfig.prefHeightProperty().bind(subFrequencyConfigPane.heightProperty());
        subFrequencyConfig.prefWidthProperty().bind(subFrequencyConfigPane.widthProperty().divide(subFrequencyConfigCount));
        subFrequencyConfig.recalibrate();

//        subFrequencyConfig.setBorder(Border.stroke(Color.RED));
    }
    @FXML
    protected void onRemSubFrequencyConfig() {
        SubFrequencyConfig remSubFrequencyConfig = subFrequencyConfigs.get(subFrequencyConfigs.size()-1);
        subFrequencyConfigCount.set(subFrequencyConfigCount.getValue()-1);
        subFrequencyConfigs.remove(remSubFrequencyConfig);
        subFrequencyConfigPane.getChildren().remove(remSubFrequencyConfig);
    }
    private int commonMultipleOfFrequencies() {
        int multiple = 1;
        for (SubFrequencyConfig frequencyConfig: subFrequencyConfigs) {
            multiple *= frequencyConfig.getFrequency();
        }
        return multiple;
    }
    private double totalVolume() {
        double volume = 0;
        for (SubFrequencyConfig frequencyConfig: subFrequencyConfigs) {
            volume += frequencyConfig.getVolume();
        }
        return volume;
    }
    private double computeValue(double secsIn, SubFrequencyConfig frequencyConfig) {
        return Math.sin(Math.PI*2*frequencyConfig.getFrequency()*secsIn)*frequencyConfig.getVolume();
//        return (Math.abs(
//                (((secsIn*frequencyConfig.getFrequency() * 4) +3) % 4) -2
//                ) - 1) * frequencyConfig.getVolume();
//        return 1*frequencyConfig.getVolume()/frequencyConfig.getFrequency();
    }
    private double medianValues(double secsIn) {
        double sum = 0;
        for (SubFrequencyConfig frequency:subFrequencyConfigs) {
            sum+=computeValue(secsIn,frequency);
//            if (computeValue(secsIn,frequency)>=1) {
//                System.out.println("computeValue("+secsIn+","+frequency+"):"+computeValue(secsIn,frequency));
//            }
        }
//        if (secsIn==0) {
//            System.out.println(Math.abs(((secsIn*subFrequencyConfigs.get(0).getFrequency() * 4 -1 ) % 4) -2) - 1);
//            System.out.println(Math.abs(((secsIn*subFrequencyConfigs.get(0).getFrequency() * 4 -1 ) % 4) -2));
//            System.out.println(((secsIn*subFrequencyConfigs.get(0).getFrequency() * 4 - 1 ) % 4) -2);
//            System.out.println("sum" + sum);
//            System.out.println("tv" + totalVolume());
//        }
        return sum/totalVolume();
    }
    protected void playShorts(short[] samples, int samplerate) throws LineUnavailableException {
        AudioFormat format = new AudioFormat(samplerate, 16, 1, true, false);
        SourceDataLine line = AudioSystem.getSourceDataLine(format);
        byte[] bytes = new byte[samples.length * 2];
        for (int i = 0; i < samples.length; i++) {
            bytes[i*2] = (byte) (samples[i] & 0xFF);
            bytes[i*2+1] = (byte) ((samples[i] >> 8) & 0xFF);
        }
//        System.out.println(samples.length);
//        System.out.println(Arrays.toString(audioValues));
//        System.out.println(Arrays.toString(samples));
//        System.out.println(Arrays.toString(bytes));
//        System.out.println(Arrays.toString(audioValues));

//        System.out.println(samples.length);
//        short prev=0;
//        System.out.println("[");
//        for (short sample:samples) {
//            System.out.printf(sample-prev+",");
//            prev=sample;
//        }
//        System.out.printf("]");

        line.open(format);
        line.start();
//        System.out.println("do");
        line.write(bytes, 0, bytes.length);
//        System.out.println("re");
        line.drain();
//        System.out.println("me");
        line.stop();
//        System.out.println("fa");
        line.close();
//        System.out.println("so");
    }

    @FXML
    protected void onPlayFile() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("playme.wav"));
        AudioFormat format = audioInputStream.getFormat();
        int sampleSizeInBits = format.getSampleSizeInBits();
        int channels = format.getChannels();
        boolean isBigEndian = format.isBigEndian();
        int sampleRate = (int) format.getSampleRate();
        System.out.println("playing wave file: encoding:" + format.getEncoding()+" sampleSize:"+sampleSizeInBits+" channels:"+channels+" bigEndian:"+isBigEndian+" sampleRate:"+sampleRate);
        System.out.println("as though it was: encoding:PCM_SIGNED sampleSize:16 channels:1 bigEndian:false sampleRate:"+sampleRate);
        byte[] buffer = new byte[audioInputStream.available()];
        audioInputStream.read(buffer);
        short[] samples = new short[buffer.length / 2];
        for (int i = 0; i < samples.length; i++) {
                samples[i] = (short) ((buffer[i * 2] & 0xFF) | (buffer[i * 2 + 1] << 8));
        }
        playShorts(samples, sampleRate);
//        System.out.println("still playing wave file: sampleSize:"+sampleSizeInBits+" channels:"+channels+" frameSize:"+frameSize);
//        System.out.println("as though it was sampleSize:16 channels:1 frameSize:2");
    }
    @FXML
    protected void onExportFile() throws UnsupportedAudioFileException, IOException {
//        AudioFileWriter writer = new AudioFileWrite
//        } AudioFileFormat.Type.WAVE AudioSystem.getAudioInputStream(new File("exported.wav"));
        int samplerate=Integer.parseInt(this.samplerate.getText());
        short[] samples = generate();
        exportShorts(samples,samplerate);

    }
    protected void exportShorts(short[] samples, int samplerate) throws IOException {
        // Define format based on provided arguments
        AudioFormat format = new AudioFormat(samplerate, 16, 1, true, false);

        // Create byte array from short array
        byte[] bytes = new byte[samples.length * 2];
        for (int i = 0; i < samples.length; i++) {
            bytes[i*2] = (byte) (samples[i] & 0xFF);
            bytes[i*2+1] = (byte) ((samples[i] >> 8) & 0xFF);
        }

        // Build AudioInputStream from format and data
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes)) {
            AudioInputStream ais = new AudioInputStream(bais, format, bytes.length / format.getFrameSize());

            // Set output format to WAVE
            AudioFileFormat.Type fileFormat = AudioFileFormat.Type.WAVE;

            // Write data to file
            try (FileOutputStream fos = new FileOutputStream("export.wav")) {
                AudioSystem.write(ais, fileFormat, fos);
            }
        }
    }
    private short[] generate() {
        int samplerate=Integer.parseInt(this.samplerate.getText());
        int seconds = Integer.parseInt(secondsPerPlay.getText());
        int totalSamples = samplerate *seconds;
        Double[] audioValues = new Double[totalSamples];
        short[] samples = new short[totalSamples];
        for (int i = 0; i < audioValues.length; i++) {
            audioValues[i]=medianValues((double) i/samplerate);
            samples[i] = (short) (audioValues[i] * Short.MAX_VALUE);
        }
        return samples;
    }
    @FXML
    protected void onGeneratePlay() throws LineUnavailableException {
//        if (false) {
//            System.out.println("conf:"+subFrequencyConfigs);
//            System.out.println("vol:"+totalVolume());
//            return;
//        }
        int samplerate=Integer.parseInt(this.samplerate.getText());
        short[] samples = generate();
        playShorts(samples,samplerate);
    }

    //@FXML
    //private Label welcomeText;

    //@FXML
    //protected void onHelloButtonClick() {
    //    welcomeText.setText("Welcome to JavaFX Application!");
    //}
}