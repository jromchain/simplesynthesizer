package com.chainsword.simplesynthesizer;

import javafx.geometry.Orientation;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class SubFrequencyConfig extends GridPane {
    TextField frequency;
    Slider volume;

    public int getFrequency() {
        return Integer.parseInt(frequency.getText());
    }
    public double getVolume() {
        return volume.getValue();
    }

    public SubFrequencyConfig() {
        recalibrate();
    }
    public void recalibrate() {
        frequency=new TextField("441");
        volume=new Slider(0,1,0.5);
        this.add(frequency,1,1);
        this.add(volume,1,2);
        frequency.setText("441");
        frequency.prefHeightProperty().bind(this.heightProperty().divide(10));
        frequency.prefWidthProperty().bind(this.widthProperty());
        volume.setOrientation(Orientation.VERTICAL);
        volume.setMin(0);
        volume.setMax(1);
        volume.setValue(0.5);
        volume.prefHeightProperty().bind(this.heightProperty().multiply(9d/10d));
        volume.prefWidthProperty().bind(this.widthProperty());
//        subFrequencyConfig.setBorder(Border.stroke(Color.RED));
    }

    @Override
    public String toString() {
        return "SubFrequencyConfig{" +
                "frequency=" + frequency.getText() +
                ", volume=" + volume.getValue() +
                ", " + super.toString() + "}";
    }

    public static void main(String[] args) {
        //put your code here

    }
}
