module com.chainsword.simplesynthesizer {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires java.desktop;

    opens com.chainsword.simplesynthesizer to javafx.fxml;
    exports com.chainsword.simplesynthesizer;
}